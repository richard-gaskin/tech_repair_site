import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faBars,
  faTimes,
  faMoneyBillWave,
  faMobileAlt,
  faLaptop,
  faDesktop,
  faPlayCircle,
  faPauseCircle,
} from '@fortawesome/free-solid-svg-icons';
import {
  faFacebookSquare,
  faInstagram,
  faPinterest,
  faSnapchat,
} from '@fortawesome/free-brands-svg-icons';
import { faBell, faUserCircle } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from './App.vue';
import router from './router';

library.add(
  faBars,
  faTimes,
  faMoneyBillWave,
  faBell,
  faUserCircle,
  faFacebookSquare,
  faInstagram,
  faPinterest,
  faSnapchat,
  faMobileAlt,
  faLaptop,
  faDesktop,
  faPlayCircle,
  faPauseCircle,
);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
